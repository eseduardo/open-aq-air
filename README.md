# README #

This README would normally document whatever steps are necessary to get your application up and running.

### 1. THE OPEN AQ AIR QUALITY
API
The Open AQ air quality API is a simple example of a free web-based API
that we can pass data to and retrieve results from. The main function of the
API, and the purpose of it for this test, is to provide the current air quality for
a city.
Documentation on the API can be found here:
https://docs.openaq.org/

### 2. THE TASK
The goal of this exercise is to build a client that interacts with this API. It
should be able to send different parameters to the API, and display the air
quality results for a given city in a friendly manner.
The client only needs to support the retrieval of data for a city, however there
are other parameters and functionality available to the API, such as sorting,
which may be useful. Potential features the client could be extended to
include for example, storing of requests and replaying them.
This is not intended to be a fully-featured solution, but more a proof of
concept that could be extended if necessary, and structured in a way that
allows for extension.
While the task could be completed in any language, given the nature of the
role at Dept, we would expect this task to be completed using C# or Java.
There are not intended to be any “gotchas” or tricks, so if you have any
queries or issues, please contact us.