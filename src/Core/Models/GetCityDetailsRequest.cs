﻿using Core.Enums;

namespace Core.Models
{
    public class GetCityDetailsRequest
    {
        public string City { get; set; }

        public string Country { get; set; }

        public Sorting Sort { get; set; }

        public int Limit { get; set; }

        public int Page { get; set; }
    }
}