﻿using System.Collections.Generic;

namespace Core.Models
{
    public class GetCityDetailsResponse
    {
        public Meta Meta { get; set; }
        public List<CityDetails> Results { get; set; }
    }
}