﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Models
{
    public class Meta
    {
        public string Name { get; set; }
        public string License { get; set; }
        public string Website { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
        public int Found { get; set; }
    }

    public class AveragingPeriod
    {
        public double Value { get; set; }
        public string Unit { get; set; }
    }

    public class Measurement
    {
        public string Parameter { get; set; }
        public double Value { get; set; }
        public DateTime LastUpdated { get; set; }
        public string Unit { get; set; }
        public string SourceName { get; set; }
        public AveragingPeriod AveragingPeriod { get; set; }
    }

    public class Coordinates
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class CityDetails
    {
        public string Location { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public List<Measurement> Measurements { get; set; }
        public double? Distance { get; set; }
        public Coordinates Coordinates { get; set; }

        public Measurement GetMax()
        {
            return Measurements.OrderBy(x => x.Value).FirstOrDefault();
        }

        public Measurement GetMin()
        {
            return Measurements.OrderBy(x => x.Value).LastOrDefault();
        }

        public int GetNumberOfDaysMeasured()
        {
            var difference = (Measurements.Max(m => m.LastUpdated) - Measurements.Min(m => m.LastUpdated)).TotalDays;
            return (int)difference;
        }

        public bool HasMeasurements => Measurements != null && Measurements.Any();
    }
}