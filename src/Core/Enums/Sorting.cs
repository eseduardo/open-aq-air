﻿namespace Core.Enums
{
    public enum Sorting
    {
        Asc,
        Desc
    }
}