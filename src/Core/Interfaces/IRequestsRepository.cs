﻿using Core.Models;
using System.Collections.Generic;

namespace Core.Interfaces
{
    public interface IRequestsRepository
    {
        void Add(GetCityDetailsRequest detailsRequest);

        List<GetCityDetailsRequest> GetLatestQueries();

        bool Exists(GetCityDetailsRequest detailsRequest);
    }
}