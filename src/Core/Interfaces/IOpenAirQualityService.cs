﻿using Core.Models;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IOpenAirQualityService
    {
        Task<GetCityDetailsResponse> GetLatestMeasurements(GetCityDetailsRequest detailsRequest);
    }
}