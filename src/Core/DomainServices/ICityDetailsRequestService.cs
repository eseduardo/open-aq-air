﻿using Core.Models;
using System.Collections.Generic;

namespace Core.DomainServices
{
    public interface ICityDetailsRequestService
    {
        List<GetCityDetailsRequest> AddAndGetLatest(GetCityDetailsRequest toAdd);
    }
}