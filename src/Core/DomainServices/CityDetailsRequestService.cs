﻿using System.Collections.Generic;
using Core.Interfaces;
using Core.Models;

namespace Core.DomainServices
{
    public class CityDetailsRequestService : ICityDetailsRequestService
    {
        private readonly IRequestsRepository _repository;

        public CityDetailsRequestService(IRequestsRepository repository)
        {
            _repository = repository;
        }
        public List<GetCityDetailsRequest> AddAndGetLatest(GetCityDetailsRequest toAdd)
        {
            if (!_repository.Exists(toAdd))
                _repository.Add(toAdd);
            return _repository.GetLatestQueries();
        }
    }
}
