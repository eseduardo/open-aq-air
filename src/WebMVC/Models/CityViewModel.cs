﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace WebMVC.Models
{
    public class CityViewModel
    {
        [Required]
        public string CityName { get; set; }

        public string Country { get; set; }

        public List<CityDetails> Results { get; set; }

        public bool HasResults => Results != null && Results.Any();

        public List<RequestViewModel> LatestQueries { get; set; }

        public bool HasQueries => LatestQueries != null && LatestQueries.Any();
    }

    public class CityDetails
    {
        public string Location { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public string CityAndCountry
        {
            get
            {
                return $"{City} ({Country})";
            }
        }

        public List<Measurement> Measurements { get; set; }

        public string MaxValue { get; set; }
        public string MinValue { get; set; }
        public int RangeOfDays { get; set; }
        public bool HasMeasurements => Measurements != null && Measurements.Any();
    }

    public class Measurement
    {
        public string Parameter { get; set; }
        public double Value { get; set; }
        public DateTime LastUpdated { get; set; }
        public string Unit { get; set; }
        public string SourceName { get; set; }
    }

    public class RequestViewModel
    {
        public string City { get; set; }

        public string Country { get; set; }

        public string CityFullName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Country))
                    return City;
                return $"{City} [{Country}]";
            }
        }
    }
}