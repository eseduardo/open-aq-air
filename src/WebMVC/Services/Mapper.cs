﻿using System.Collections.Generic;
using System.Linq;
using WebMVC.Models;
using CORE = Core.Models;

namespace WebMVC.Services
{
    public static class Mapper
    {
        public static List<CityDetails> Map(IEnumerable<CORE.CityDetails> resultCityDetails)
        {
            return resultCityDetails.Select(Map).ToList();
        }

        public static List<RequestViewModel> Map(List<CORE.GetCityDetailsRequest> latestQueries)
        {
            return latestQueries.Select(Map).ToList();
        }

        private static RequestViewModel Map(CORE.GetCityDetailsRequest query)
        {
            return new RequestViewModel
            {
                City = query.City,
                Country = query.Country,
            };
        }

        private static CityDetails Map(CORE.CityDetails resultCityDetails)
        {
            return new CityDetails
            {
                Location = resultCityDetails.Location,
                City = resultCityDetails.City,
                Country = resultCityDetails.Country,
                Measurements = Map(resultCityDetails.Measurements),
                MaxValue = resultCityDetails.GetMax().Value.ToString(),
                MinValue = resultCityDetails.GetMin().Value.ToString(),
                RangeOfDays = resultCityDetails.GetNumberOfDaysMeasured(),
            };
        }

        private static List<Measurement> Map(IEnumerable<CORE.Measurement> measurements)
        {
            return measurements.Select(Map).ToList();
        }

        private static Measurement Map(CORE.Measurement measurement)
        {
            return new Measurement
            {
                Parameter = measurement.Parameter,
                Value = measurement.Value,
                LastUpdated = measurement.LastUpdated,
                Unit = measurement.Unit,
                SourceName = measurement.SourceName,
            };
        }
    }
}