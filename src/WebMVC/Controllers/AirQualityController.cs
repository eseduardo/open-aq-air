﻿using Core.DomainServices;
using Core.Enums;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebMVC.Models;
using WebMVC.Services;
using CORE = Core.Models;

namespace WebMVC.Controllers
{
    public class AirQualityController : Controller
    {
        private readonly IOpenAirQualityService _aqService;
        private readonly ICityDetailsRequestService _cityDetailsRequestService;

        public AirQualityController(IOpenAirQualityService aqService, ICityDetailsRequestService cityDetailsRequestService)
        {
            _aqService = aqService;
            _cityDetailsRequestService = cityDetailsRequestService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Details(CityViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = new CORE.GetCityDetailsRequest
                {
                    Limit = 100,
                    City = model.CityName,
                    Country = model.Country,
                    Page = 1,
                    Sort = Sorting.Asc
                };
                var latestQueries = _cityDetailsRequestService.AddAndGetLatest(request);
                model.LatestQueries = Mapper.Map(latestQueries);

                var measurements = await _aqService.GetLatestMeasurements(request);
                model.Results = Mapper.Map(measurements.Results);
            }

            return View("Index", model);
        }
    }
}