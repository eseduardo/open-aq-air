﻿using System;
using System.Collections.Generic;

namespace Infraestructure.Caching
{
    //https://codereview.stackexchange.com/questions/35864/a-simple-cache-class

    public class SuperSimpleCacheService : ICacheService
    {
        private static readonly Dictionary<string, CacheItem> _cache;

        static SuperSimpleCacheService()
        {
            _cache = new Dictionary<string, CacheItem>();
        }

        public void Add(string key, object value)
        {
            _cache.Add(key, new CacheItem { Value = value, Created = DateTime.Now.Ticks });
        }

        public object Get(string key)
        {
            if (_cache.ContainsKey(key))
            {
                if (_cache[key].Created + 1000000000 > DateTime.Now.Ticks) //300000 was chosen by magic
                {
                    return _cache[key].Value;
                }
                _cache.Remove(key);
            }
            return null;
        }

        internal sealed class CacheItem
        {
            public object Value { get; set; }
            public long Created { get; set; }
        }
    }
}