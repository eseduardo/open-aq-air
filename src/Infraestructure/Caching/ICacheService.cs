﻿namespace Infraestructure.Caching
{
    public interface ICacheService
    {
        void Add(string key, object value);

        object Get(string key);
    }
}