﻿namespace Infraestructure.Configuration
{
    public interface IInfrastructureConfiguration
    {
        string OpenAQUrl { get; }
    }
}