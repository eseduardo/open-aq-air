﻿namespace Infraestructure.Configuration
{
    public class InfrastructureConfiguration : IInfrastructureConfiguration
    {
        public string OpenAQUrl { get; } = "https://api.openaq.org";
    }
}