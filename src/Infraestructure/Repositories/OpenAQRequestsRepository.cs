﻿using Core.Interfaces;
using Core.Models;
using System.Collections.Generic;
using System.Linq;

namespace Infraestructure.Repositories
{
    public class InMemoryOpenAqRequestsRepository : IRequestsRepository
    {
        private readonly List<GetCityDetailsRequest> _entities = new List<GetCityDetailsRequest>();

        public void Add(GetCityDetailsRequest detailsRequest)
        {
            _entities.Add(detailsRequest);
        }

        public bool Exists(GetCityDetailsRequest detailsRequest)
        {
            return _entities.Any(dr=> dr.City == detailsRequest.City && dr.Country == detailsRequest.Country);
        }

        public List<GetCityDetailsRequest> GetLatestQueries()
        {
            return _entities;
        }
    }
}