﻿using Core.Models;

namespace Infraestructure.Extensions
{
    public static class Extensions{
        public static string GenerateKey(this GetCityDetailsRequest detailsRequest)
        {
            return $"City{detailsRequest.City}Country{detailsRequest.Country}Sort{detailsRequest.Sort}Limit{detailsRequest.Limit}Page{detailsRequest.Page}";
        }

    }
}