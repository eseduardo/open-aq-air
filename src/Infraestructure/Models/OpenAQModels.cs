﻿using System;
using System.Collections.Generic;

namespace Infraestructure.Models
{
    public class Meta
    {
        public string name { get; set; }
        public string license { get; set; }
        public string website { get; set; }
        public int page { get; set; }
        public int limit { get; set; }
        public int found { get; set; }
    }

    public class AveragingPeriod
    {
        public double value { get; set; }
        public string unit { get; set; }
    }

    public class Measurement
    {
        public string parameter { get; set; }
        public double value { get; set; }
        public DateTime lastUpdated { get; set; }
        public string unit { get; set; }
        public string sourceName { get; set; }
        public AveragingPeriod averagingPeriod { get; set; }
    }

    public class Coordinates
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }

    public class Result
    {
        public string location { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public List<Measurement> measurements { get; set; }
        public double? distance { get; set; }
        public Coordinates coordinates { get; set; }
    }

    public class RootObject
    {
        public Meta meta { get; set; }
        public List<Result> results { get; set; }
    }
}