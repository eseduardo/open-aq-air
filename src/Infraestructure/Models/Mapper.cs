﻿using System.Collections.Generic;
using System.Linq;
using CORE = Core.Models;

namespace Infraestructure.Models
{
    public static class Mapper
    {
        public static CORE.GetCityDetailsResponse Map(Models.RootObject rootObject)
        {
            var mapped = new CORE.GetCityDetailsResponse
            {
                Results = Map(rootObject.results),
                Meta = Map(rootObject.meta)
            };
            return mapped;
        }

        private static CORE.Meta Map(Meta source)
        {
            return new CORE.Meta
            {
                Name = source.name,
                License = source.name,
                Website = source.website,
                Page = source.page,
                Limit = source.limit,
                Found = source.found
            };
        }

        private static List<CORE.CityDetails> Map(List<Result> source)
        {
            return source.Select(Map).ToList();
        }

        private static CORE.CityDetails Map(Result source)
        {
            return new CORE.CityDetails
            {
                Location = source.location,
                City = source.city,
                Country = source.country,
                Measurements = Map(source.measurements),
            };
        }

        private static List<CORE.Measurement> Map(List<Measurement> source)
        {
            return source.Select(Map).ToList();
        }

        private static CORE.Measurement Map(Measurement source)
        {
            return new CORE.Measurement
            {
                Parameter = source.parameter,
                Value = source.value,
                LastUpdated = source.lastUpdated,
                Unit = source.unit,
                SourceName = source.sourceName,
            };
        }
    }
}