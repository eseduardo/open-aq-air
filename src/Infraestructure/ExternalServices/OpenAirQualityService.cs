﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Models;
using Infraestructure.Caching;
using Infraestructure.Configuration;
using Infraestructure.Extensions;
using Infraestructure.Models;
using Newtonsoft.Json;

namespace Infraestructure.ExternalServices
{
    public class OpenAirQualityService : IOpenAirQualityService
    {
        private readonly ICacheService _cacheService;
        private readonly HttpClient _httpClient;
        private readonly string _remoteServiceBaseUrl;

        public OpenAirQualityService(ICacheService cacheService, IInfrastructureConfiguration configuration, HttpClient httpClient)
        {
            _cacheService = cacheService;
            _httpClient = httpClient;
            _remoteServiceBaseUrl = $"{configuration.OpenAQUrl}/v1/";
        }

        public async Task<GetCityDetailsResponse> GetLatestMeasurements(GetCityDetailsRequest detailsRequest)
        {
            try
            {
                var cached = _cacheService.Get(detailsRequest.GenerateKey());
                if (cached != null)
                {
                    return (GetCityDetailsResponse)cached;
                }

                var response = await GetApiLatest(detailsRequest);

                _cacheService.Add(detailsRequest.GenerateKey(), response);
                return response;
            }
            catch (Exception ex)
            {
                //log.Error("GetLatest", ex);
                throw;
            }
        }

        private async Task<GetCityDetailsResponse> GetApiLatest(GetCityDetailsRequest detailsRequest)
        {
            var uri = API.Latest.Get(_remoteServiceBaseUrl, detailsRequest);

            var responseString = await _httpClient.GetStringAsync(uri);

            var response = JsonConvert.DeserializeObject<RootObject>(responseString);

            return Mapper.Map(response);
        }
    }
}