﻿using Core.Models;
using System.Text;

namespace Infraestructure.ExternalServices
{
    public static class API
    {
        public class Latest
        {
            public static string Get(string baseUri, GetCityDetailsRequest detailsRequest)
            {
                var uri = new StringBuilder();
                uri.Append($"{baseUri}latest?page={detailsRequest.Page}&limit={detailsRequest.Limit}city={detailsRequest.City}&sort={detailsRequest.Sort}");
                if (!string.IsNullOrWhiteSpace(detailsRequest.City))
                    uri.Append($"&city={detailsRequest.City}");
                if (!string.IsNullOrWhiteSpace(detailsRequest.Country))
                    uri.Append($"&country={detailsRequest.Country}");

                return uri.ToString();
            }
        }
    }
}